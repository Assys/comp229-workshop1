# Task 0

Clone this repository (well done!)

# Task 1

Take a look at the two repositories:

  * (A) https://bitbucket.org/altmattr/personalised-correlation/src/master/
  * (B) https://github.com/Whiley/WhileyCompiler

And answer the following questions about them:

  * These repositories are at two different websites - github and bitbucket - what are these sites?  What service do they provide? Which is better?
  * Who made the last commit to repository A? - EMMA WALKER
  * Who made the first commit to repository A? - JON MOUNTJOY
  * Who made the first and last commits to repository B? FIRST = DAVID PEARCE,,  LAST = DAVID PEARCE
  * Are either/both of these projects active at the moment?  🤔 If not, what do you think happened?
  A IS STILL ACTIVE, REP B MOVED TO A DIFFERENT REPOSITORY
  * 🤔 Which file in each project has had the most activity?
  A = app.py
  B = modules
  

# Task 2

Setup a new IntelliJ project with a main method that will print the following message to the console when run:

~~~~~
Sheep and Wolves
~~~~~